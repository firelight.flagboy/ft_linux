#!/bin/bash
src=$1
dest=$2
interval=${3:-1}
cmp=$(mktemp --suffix=.diff)

log() {
    local level=$1 msg=$2 output=${3:-'/dev/stdout'}
    printf "\e[2m%s\e[0m : %6s : %s\n" "$(date --iso-8601=seconds)" "$level" "$msg" >$output
}

log_info() {
    local INFO="INFO"
    log INFO "$*"
}

log_error() {
    local ERROR="ERROR"
    log ERROR "$*" /dev/stderr
}

trap "echo signaled; rm $cmp; exit" SIGHUP SIGINT SIGTERM

log_info "sync $src -> $dest"
log_info "interval: $interval sec"

if [ ! -f "$src" ]; then
    log_error "$src doesn't exist"
    exit 1
fi
if [ -z "$dest" ]; then
    log_error "need a target"
    exit 1
fi

size_src=$(wc -c $src | cut -d' ' -f1)

log_info "size $src: $size_src"

while true; do
    if [ ! -f "$dest" ]; then
        log_info "$dest doesn't exist, copying"
        cp $src $dest
    else
        diff $dest $src > $cmp
        diff_res=$?
        if [ $diff_res -ne 0 ]; then
            size_diff=$(wc -c $cmp | cut -d' ' -f1)
            log_info "diff size: $size_diff"
            log_info "patching file $dest"
            patch -i $cmp $dest
        else
            log_info "file not changed"
        fi
    fi
    sleep $interval
done

rm $cmp
