DIR := \
	/boot \
	/home \
	/mnt \
	/opt \
	/srv \
	$(shell echo /etc/{opt,sysconfig}) \
	/lib/firmware \
	$(shell echo /media/{floppy,cdrom}) \
	$(shell echo /usr/{,local/}{bin,include,lib,sbin,src}) \
	$(shell echo /usr/{,local/}share/{color,dict,doc,info,locale,man}) \
	$(shell echo /usr/{,local/}share/{misc,terminfo,zoneinfo}) \
	$(shell echo /usr/{,local/}share/man/man{1..8}) \
	$(shell echo /var/{cache,local,log,mail,opt,spool}) \
	$(shell echo /var/lib/{color,misc,locate}) \
	/var/lib/hwclock

VAR_LINK := /var/run /var/lock /etc/mtab
DIR_PERM := /root /tmp /var/tmp
LOG_FILES := $(shell echo /var/log/{btmp,lastlog,faillog,wtmp})

FILES := \
	/etc/hosts \
	/etc/passwd \
	/etc/group \
	$(LOG_FILES)

all: $(DIR) $(VAR_LINK) $(DIR_PERM) $(FILES)

$(DIR):
	mkdir -p $@

print-%:
	echo $($*)

/var/run:
	ln -sf /run $@

/etc/mtab:
	ln -s /proc/self/mounts /etc/mtab

/var/lock:
	ln -sf /run/lock $@

.PHONY: $(DIR_PERM)
/root:
	install -dv -m 0750 $@

/tmp /var/tmp:
	install -dv -m 1777 $@


$(LOG_FILES):
	touch $@

/var/log/lastlog:
	touch $@
	chgrp utmp $@
	chmod 664 $@

/var/log/btmp:
	touch $@
	chmod 600 $@


/etc/hosts:
	echo "127.0.0.1 localhost $$(hostname)" > /etc/hosts

.ONESHELL: /etc/passwd /etc/group

/etc/passwd:
	cat > /etc/passwd <<- 'EOF'
		root:x:0:0:root:/root:/bin/bash
		bin:x:1:1:bin:/dev/null:/bin/false
		daemon:x:6:6:Daemon User:/dev/null:/bin/false
		messagebus:x:18:18:D-Bus Message Daemon User:/run/dbus:/bin/false
		systemd-bus-proxy:x:72:72:systemd Bus Proxy:/:/bin/false
		systemd-journal-gateway:x:73:73:systemd Journal Gateway:/:/bin/false
		systemd-journal-remote:x:74:74:systemd Journal Remote:/:/bin/false
		systemd-journal-upload:x:75:75:systemd Journal Upload:/:/bin/false
		systemd-network:x:76:76:systemd Network Management:/:/bin/false
		systemd-resolve:x:77:77:systemd Resolver:/:/bin/false
		systemd-timesync:x:78:78:systemd Time Synchronization:/:/bin/false
		systemd-coredump:x:79:79:systemd Core Dumper:/:/bin/false
		uuidd:x:80:80:UUID Generation Daemon User:/dev/null:/bin/false
		nobody:x:99:99:Unprivileged User:/dev/null:/bin/false
	EOF

/etc/group:
	cat > $@ <<- 'EOF'
		root:x:0:
		bin:x:1:daemon
		sys:x:2:
		kmem:x:3:
		tape:x:4:
		tty:x:5:
		daemon:x:6:
		floppy:x:7:
		disk:x:8:
		lp:x:9:
		dialout:x:10:
		audio:x:11:
		video:x:12:
		utmp:x:13:
		usb:x:14:
		cdrom:x:15:
		adm:x:16:
		messagebus:x:18:
		systemd-journal:x:23:
		input:x:24:
		mail:x:34:
		kvm:x:61:
		systemd-bus-proxy:x:72:
		systemd-journal-gateway:x:73:
		systemd-journal-remote:x:74:
		systemd-journal-upload:x:75:
		systemd-network:x:76:
		systemd-resolve:x:77:
		systemd-timesync:x:78:
		systemd-coredump:x:79:
		uuidd:x:80:
		wheel:x:97:
		nogroup:x:99:
		users:x:999:
	EOF

.PHONY: add-tester-user remove-tester-user

add-tester-user:
	echo "tester:x:$$(ls -n $$(tty) | cut -d" " -f3):101::/home/tester:/bin/bash" >> /etc/passwd
	echo "tester:x:101:" >> /etc/group
	install -o tester -d /home/tester

remove-tester-user:
