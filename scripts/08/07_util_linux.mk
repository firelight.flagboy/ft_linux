PACKAGE_NAME := util-linux-2.36.2
PACKAGE_ARCH := $(PACKAGE_NAME).tar.xz
BUILD_MAKE := $(PACKAGE_NAME)/Makefile
BIN := fdisk
TO_BUILD := $(addprefix $(PACKAGE_NAME)/, $(BIN))
INSTALL_DIR := /usr/bin
TO_INSTALL := $(addprefix /sbin/, $(BIN))

.PHONY: all
all: $(TO_INSTALL)

$(PACKAGE_NAME): $(PACKAGE_ARCH)
	tar xf $^

$(PACKAGE_NAME)/configure: $(PACKAGE_NAME)

/var/lib/hwclock:
	mkdir -p $@

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_NAME)/configure | /var/lib/hwclock
	cd $(PACKAGE_NAME)
	./configure ADJTIME_PATH=/var/lib/hwclock/adjtime \
		--docdir=/usr/share/doc/util-linux-2.36.2 \
		--disable-chfn-chsh \
		--disable-login \
		--disable-nologin \
		--disable-su \
		--disable-setpriv \
		--disable-runuser \
		--disable-pylibmount \
		--disable-static \
		--without-python \
		runstatedir=/run

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -j -C $(PACKAGE_NAME)

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_NAME) install
