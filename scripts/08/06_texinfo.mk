PACKAGE_NAME := texinfo-6.7
PACKAGE_ARCH := $(PACKAGE_NAME).tar.xz
BUILD_MAKE := $(PACKAGE_NAME)/Makefile
BIN := texi2any
TO_BUILD := $(addprefix $(PACKAGE_NAME)/tp/, $(BIN))
INSTALL_DIR := /usr/bin
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(BIN))

.PHONY: all
all: $(TO_INSTALL)

$(PACKAGE_NAME): $(PACKAGE_ARCH)
	tar xf $^

$(PACKAGE_NAME)/configure: $(PACKAGE_NAME)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_NAME)/configure
	cd $(PACKAGE_NAME)
	./configure --prefix=/usr

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -j -C $(PACKAGE_NAME)

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_NAME) install
