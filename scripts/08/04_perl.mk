PACKAGE_NAME := perl-5.32.1
PACKAGE_ARCH := $(PACKAGE_NAME).tar.xz
BUILD_MAKE := $(PACKAGE_NAME)/Makefile
BIN := perl
TO_BUILD := $(addprefix $(PACKAGE_NAME)/, $(BIN))
INSTALL_DIR := /usr/bin
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(BIN))

.PHONY: all
all: $(TO_INSTALL)

$(PACKAGE_NAME): $(PACKAGE_ARCH)
	tar xf $^

$(PACKAGE_NAME)/Configure: $(PACKAGE_NAME)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_NAME)/Configure
	cd $(PACKAGE_NAME)
	sh Configure -des                                \
		-Dprefix=/usr                                \
		-Dvendorprefix=/usr                          \
		-Dprivlib=/usr/lib/perl5/5.32/core_perl      \
		-Darchlib=/usr/lib/perl5/5.32/core_perl      \
		-Dsitelib=/usr/lib/perl5/5.32/site_perl      \
		-Dsitearch=/usr/lib/perl5/5.32/site_perl     \
		-Dvendorlib=/usr/lib/perl5/5.32/vendor_perl  \
		-Dvendorarch=/usr/lib/perl5/5.32/vendor_perl

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -j -C $(PACKAGE_NAME)

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_NAME) install
