PACKAGE_DIR := gettext-0.21
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_MAKE := $(PACKAGE_DIR)/Makefile
BIN := msgfmt msgmerge xgettext
TO_BUILD := $(addprefix $(PACKAGE_DIR)/gettext-tools/src/, $(BIN))
INSTALL_DIR := /usr/bin
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(BIN))

.PHONY: all
all: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $^

$(PACKAGE_DIR)/configure: $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure
	cd $(PACKAGE_DIR)
	./configure --disable-shared

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -j -C $(PACKAGE_DIR)

$(TO_INSTALL): $(TO_BUILD)
	cp $^ $(INSTALL_DIR)
