PACKAGE_DIR := gcc-10.2.0
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)/build-v2
BUILD_MAKE := $(BUILD_DIR)/Makefile
LIB := $(BUILD_DIR)/src/.libs/libstdc++.a
LIB_INSTALLED := /usr/lib/libstdc++.a

.PHONY: all
all: \
	$(PACKAGE_DIR) \
	$(PACKAGE_DIR)/libgcc/gthr-default.h \
	$(LIB_INSTALLED)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $^

$(PACKAGE_DIR)/libgcc/gthr-default.h: $(PACKAGE_DIR)/libgcc/gthr-posix.h | $(PACKAGE_DIR)
	ln -s gthr-posix.h $@

$(BUILD_DIR): | $(PACKAGE_DIR)
	mkdir -p $@

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/libstdc++-v3/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	../libstdc++-v3/configure             \
		CXXFLAGS="-g -O2 -D_GNU_SOURCE"   \
		--prefix=/usr                     \
		--disable-multilib                \
		--disable-nls                     \
		--host=$$(uname -m)-lfs-linux-gnu \
		--disable-libstdcxx-pch

$(LIB): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(LIB_INSTALLED): $(LIB)
	$(MAKE) -C $(BUILD_DIR) -j install
