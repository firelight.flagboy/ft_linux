PACKAGE_DIR := make-ca-1.7
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile

TARGET := make-ca
TO_INSTALL := $(addprefix /usr/sbin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(BUILD_MAKE) : | $(PACKAGE_DIR)

$(TO_INSTALL): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j install
	install -vdm755 /etc/ssl/local
	/usr/sbin/make-ca -g
	systemctl enable update-pki.timer
