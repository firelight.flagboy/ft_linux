PACKAGE_DIR := llvm-project-12.0.1rc2.src
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)/build
BUILD_MAKE := $(BUILD_DIR)/build.ninja
CHECK_RESULT := $(BUILD_DIR)/check-result.out

BIN_TARGET := clang clang-12 ld.lld
LIB_TARGET := libclang.so libclang.so.12
TO_BUILD := \
	$(addprefix $(BUILD_DIR)/build/bin/, $(BIN_TARGET)) \
	$(addprefix $(BUILD_DIR)/build/lib/, $(LIB_TARGET))

TO_INSTALL := \
	$(addprefix /usr/bin/, $(BIN_TARGET)) \
	$(addprefix /usr/lib/, $(LIB_TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<

	# correct python script \
	( \
		cd $(PACKAGE_DIR) && \
		grep -rl '#!.*python' | xargs sed -i '1s/python$/python3/' \
	)

	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

$(BUILD_DIR): $(PACKAGE_DIR)
	mkdir -p $(BUILD_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR) && \
		CC=gcc CXX=g++ \
		cmake -DCMAKE_INSTALL_PREFIX=/usr           \
      -DLLVM_ENABLE_FFI=ON                      \
      -DCMAKE_BUILD_TYPE=Release                \
      -DLLVM_BUILD_LLVM_DYLIB=ON                \
      -DLLVM_LINK_LLVM_DYLIB=ON                 \
      -DLLVM_ENABLE_RTTI=ON                     \
			-DLLVM_ENABLE_PROJECTS="lld;clang" \
      -DLLVM_TARGETS_TO_BUILD="host;AMDGPU;BPF" \
      -DLLVM_BUILD_TESTS=ON                     \
      -DLLVM_BINUTILS_INCDIR=/usr/include       \
      -Wno-dev -G Ninja ../llvm

$(TO_BUILD): $(BUILD_MAKE)
	cd $(BUILD_DIR) && ninja

$(CHECK_RESULT): $(BUILD_MAKE) $(TO_BUILD)
	(cd $(BUILD_DIR) && ninja check-all 2>&1) | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	cd $(BUILD_DIR) && ninja install
