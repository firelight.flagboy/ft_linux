PACKAGE_DIR := wget-1.21.1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := wget
TO_BUILD := $(addprefix $(BUILD_DIR)/src/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--with-ssl=openssl

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
