PACKAGE_DIR := cmake-3.20.5
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := cmake ccmake
TO_BUILD := $(addprefix $(BUILD_DIR)/bin, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/bootstrap : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/bootstrap | $(BUILD_DIR)
	cd $(BUILD_DIR)
	sed -i '/"lib64"/s/64//' Modules/GNUInstallDirs.cmake
	./bootstrap --prefix=/usr        \
							--system-libs        \
							--mandir=/share/man  \
							--no-system-jsoncpp  \
							--no-system-librhash \
							--docdir=/share/doc/cmake-3.20.5

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j4

$(CHECK_RESULT): $(BUILD_MAKE) $(TO_BUILD)
	(cd $(BUILD_DIR) && \
		su tester -c "LC_ALL=en_US.UTF-8 bin/ctest -j4 -O /dev/stdout") | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
