PACKAGE_DIR := openssh-8.4p1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := scp ssh ssh-add ssh-agent ssh-keygen ssh-copy-id sshd
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(filter-out ssh-copy-id, $(TARGET)) contrib/ssh-copy-id)
TO_INSTALL := \
	$(addprefix /usr/bin/, $(filter-out sshd, $(TARGET))) \
	/usr/sbin/sshd

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

.ONESHELL: $(PACKAGE_DIR)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

	cd $@

	install  -v -m700 -d /var/lib/sshd
	chown    -v root:sys /var/lib/sshd

	groupadd -g 50 sshd
	useradd  -c 'sshd PrivSep' \
			-d /var/lib/sshd \
			-g sshd \
			-s /bin/false \
			-u 50 sshd

	sed -e '/INSTALLKEYS_SH/s/)//' -e '260a\  )' -i contrib/ssh-copy-id

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--sysconfdir=/etc/ssh \
		--with-md5-passwords \
		--with-privsep-path=/var/lib/sshd

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(BUILD_MAKE)
	cp $(BUILD_DIR)/scp /usr/bin/scp
	$(MAKE) -C $(BUILD_DIR) -j1 tests 2>&1 | tee $@

.ONESHELL: $(TO_INSTALL)
$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install

	cd $(BUILD_DIR)

	install -v -m755	contrib/ssh-copy-id /usr/bin

	install -v -m644	contrib/ssh-copy-id.1 \
						/usr/share/man/man1
	install -v -m755 -d /usr/share/doc/$(PACKAGE_DIR)
	install -v -m644	INSTALL LICENCE OVERVIEW README* \
						/usr/share/doc/$(PACKAGE_DIR)
