PACKAGE_DIR := curl-7.75.0
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := curl
TO_BUILD := $(addprefix $(BUILD_DIR)/src/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

.ONESHELL: $(PACKAGE_DIR)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

	cd $@

	grep -rl '#!.*python$$' | xargs sed -i '1s/python/&3/'

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--disable-static \
		--enable-threaded-resolver \
		--with-ca-path=/etc/ssl/certs

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) test 2>&1 | tee $@

.ONESHELL: $(TO_INSTALL)
$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install

	cd $(BUILD_DIR)
	rm -rf docs/examples/.deps

	find docs \( -name Makefile\* -o -name \*.1 -o -name \*.3 \) -exec rm {} \;

	install -v -d -m755 /usr/share/doc/curl-7.75.0
	cp -v -R docs/*     /usr/share/doc/curl-7.75.0
