PACKAGE_DIR := p11-kit-0.23.22
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := update-ca-certificates trust p11-kit
TO_BUILD := $(addprefix $(BUILD_DIR)/, p11-kit/p11-kit trust/trust)
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

.ONESHELL: $(PACKAGE_ARCH)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	cd $@
	sed '20,$ d' -i trust/trust-extract-compat
	cat >> trust/trust-extract-compat << "EOF"
	# Copy existing anchor modifications to /etc/ssl/local
	/usr/libexec/make-ca/copy-trust-modifications

	# Generate a new trust store
	/usr/sbin/make-ca -f -g
	EOF

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--with-trust-paths=/etc/pki/anchors

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(BUILD_MAKE)
	su tester -c "PATH=$$PATH $(MAKE) -C $(BUILD_DIR) check" 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
	ln -sfv /usr/libexec/p11-kit/trust-extract-compat \
		/usr/bin/update-ca-certificates
	ln -sfv ./pkcs11/p11-kit-trust.so /usr/lib/libnssckbi.so
