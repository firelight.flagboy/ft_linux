PACKAGE_DIR := sudo-1.9.5p2
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/../sudo-check-result.out

TARGET := sudo visudo
TO_BUILD := $(addprefix $(BUILD_DIR)/src/, $(TARGET))
TO_INSTALL := \
	/usr/bin/sudo \
	/usr/sbin/visudo

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

	touch $(PACKAGE_DIR)/configure

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--libexecdir=/usr/lib \
		--with-secure-path \
		--with-all-insults \
		--with-env-editor \
		--docdir=/usr/share/doc/$(PACKAGE_DIR) \
		--with-passprompt="[sudo] password for %p: "

$(TO_BUILD): $(BUILD_MAKE)
	cd $(BUILD_DIR) && $(MAKE) -j

$(CHECK_RESULT): $(BUILD_MAKE)
	env LC_ALL=C $(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@
	grep failed $(CHECK_RESULT)

.ONESHELL: $(TO_INSTALL)
$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
	ln -sfv libsudo_util.so.0.0.0 /usr/lib/sudo/libsudo_util.so.0

	mkdir -vp /etc/sudoers.d
	cat > /etc/sudoers.d/sudo << "EOF"
	Defaults secure_path="/usr/bin:/bin:/usr/sbin:/sbin"
	%wheel ALL=(ALL) ALL
	EOF
