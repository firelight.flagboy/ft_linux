PACKAGE_DIR := blfs-systemd-units-20210122
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz

.PHONY: all install-sshd
all: install-sshd | $(PACKAGE_DIR)

install-sshd: /lib/systemd/system/sshd.service
	$(MAKE) -C $(PACKAGE_DIR) $@

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
