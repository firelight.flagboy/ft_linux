PACKAGE_DIR := tcl8.6.11
PACKAGE_ARCH := $(PACKAGE_DIR)-src.tar.gz
TARGET := tclsh8.6
BUILD_DIR := $(PACKAGE_DIR)/unix
TO_BUILD := $(BUILD_DIR)/tclsh
TO_INSTALL := \
	/usr/bin/tclsh8.6 \
	/usr/lib/libtcl8.6.so \

BUILD_MAKE := $(BUILD_DIR)/Makefile
SRC_DIR := /sources/$(PACKAGE_DIR)
HEADERS := \
	itcl.h \
	itcl2TclOO.h \
	itclDecls.h \
	itclInt.h \
	itclIntDecls.h \
	itclMigrate2TclCore.h \
	itclTclIntStubsFcn.h \
	tcl.h \
	tclDecls.h \
	tclInt.h \
	tclIntDecls.h \
	tclIntPlatDecls.h \
	tclOO.h \
	tclOODecls.h \
	tclOOInt.h \
	tclOOIntDecls.h \
	tclPlatDecls.h \
	tclPort.h \
	tclThread.h \
	tclTomMath.h \
	tclTomMathDecls.h \
	tclUnixPort.h \

HDRS_TO_INSTALL := $(addprefix /usr/include/, $(HEADERS))

.PHONY: all clean
all: \
	$(PACKAGE_DIR)/html \
	$(TO_INSTALL) \
	$(HDRS_TO_INSTALL) \

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/html: $(PACKAGE_DIR)-html.tar.gz |$(PACKAGE_DIR)
	cd $(PACKAGE_DIR) && \
		tar xf ../$< --strip-components=1
	touch html

$(BUILD_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(BUILD_DIR)/configure
	cd $(PACKAGE_DIR)
	SRCDIR=$(SRC_DIR)
	cd unix
	./configure --prefix=/usr \
		--mandir=/usr/share/man \
		$$([ "$$(uname -m)" = x86_64 ] && echo --enable-64bit)

$(TO_BUILD): $(BUILD_MAKE)
	SRCDIR=$(SRC_DIR) \
		$(MAKE) -C $(BUILD_DIR) -j

	sed -e "s|$(SRC_DIR)/unix|/usr/lib|" \
		-e "s|$(SRC_DIR)|/usr/include|" \
		-i tclConfig.sh

	sed -e "s|$(SRC_DIR)/unix/pkgs/tdbc1.1.2|/usr/lib/tdbc1.1.2|" \
		-e "s|$(SRC_DIR)/pkgs/tdbc1.1.2/generic|/usr/include|" \
		-e "s|$(SRC_DIR)/pkgs/tdbc1.1.2/library|/usr/lib/tcl8.6|" \
		-e "s|$(SRC_DIR)/pkgs/tdbc1.1.2|/usr/include|" \
		-i pkgs/tdbc1.1.2/tdbcConfig.sh

	sed -e "s|$(SRC_DIR)/unix/pkgs/itcl4.2.1|/usr/lib/itcl4.2.1|" \
		-e "s|$(SRC_DIR)/pkgs/itcl4.2.1/generic|/usr/include|" \
		-e "s|$(SRC_DIR)/pkgs/itcl4.2.1|/usr/include|" \
		-i pkgs/itcl4.2.1/itclConfig.sh

$(BUILD_DIR)/check-result.out: $(BUILD_MAKE) $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) -j test > $@

$(TO_INSTALL): $(TO_BUILD) $(BUILD_DIR)/check-result.out
	$(MAKE) -C $(BUILD_DIR) -j install
	chmod -v u+w /usr/lib/libtcl8.6.so
	ln -sfv tclsh8.6 /usr/bin/tclsh
	mv -v /usr/share/man/man3/{Thread,Tcl_Thread}.3

$(HDRS_TO_INSTALL): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) -j install-private-headers
