PACKAGE_DIR := libcap-2.48
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := capsh getcap getpcaps setcap
TO_BUILD := $(addprefix $(BUILD_DIR)/progs/, $(TARGET))
TO_INSTALL := $(addprefix /usr/sbin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	sed -i '/install -m.*STA/d' $(PACKAGE_DIR)/libcap/Makefile

$(BUILD_MAKE): | $(PACKAGE_DIR)

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j prefix=/usr lib=lib

$(CHECK_RESULT): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) test 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j prefix=/usr lib=lib install
	for libname in cap psx; do \
		mv -v /usr/lib/lib$${libname}.so.* /lib; \
		ln -sfv ../../lib/lib$${libname}.so.2 /usr/lib/lib$${libname}.so; \
		chmod -v 755 /lib/lib$${libname}.so.2.48; \
	done
