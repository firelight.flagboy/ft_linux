PACKAGE_DIR := m4-1.4.18
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
TARGET := m4
TO_BUILD := $(addprefix $(PACKAGE_DIR)/src/, $(TARGET))
INSTALL_DIR := /usr/bin
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(TARGET))

.PHONY: all clean
all: $(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	cd $(PACKAGE_DIR) && \
		sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c && \
		echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

$(PACKAGE_DIR)/Makefile: $(PACKAGE_DIR)/configure
	cd $(PACKAGE_DIR) && ./configure --prefix=/usr

$(PACKAGE_DIR)/check-result.out: $(PACKAGE_DIR)/Makefile $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_DIR) check > $@

$(TO_BUILD): $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR)

$(TO_INSTALL): $(PACKAGE_DIR)/check-result.out
	$(MAKE) -j -C $(PACKAGE_DIR) install
