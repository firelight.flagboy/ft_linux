PACKAGE_DIR := bc-3.3.0
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
TARGET := bc dc
TO_BUILD := $(addprefix $(PACKAGE_DIR)/bin/, $(TARGET))
INSTALL_DIR := /usr/bin
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(TARGET))

.PHONY: all clean
all: $(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

$(PACKAGE_DIR)/Makefile: $(PACKAGE_DIR)/configure
	cd $(PACKAGE_DIR) && PREFIX=/usr CC=gcc ./configure -G -O3

$(PACKAGE_DIR)/check-result.out: $(PACKAGE_DIR)/Makefile $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_DIR) test > $@

$(TO_BUILD): $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR)

$(TO_INSTALL): $(PACKAGE_DIR)/check-result.out
	$(MAKE) -j -C $(PACKAGE_DIR) install
