PACKAGE_DIR := flex-2.6.4
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
TARGET := flex
TO_BUILD := $(addprefix $(PACKAGE_DIR)/src/, $(TARGET))
INSTALL_DIR := /usr/bin
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(TARGET))

.PHONY: all clean
all: $(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

$(PACKAGE_DIR)/Makefile: $(PACKAGE_DIR)/configure
	cd $(PACKAGE_DIR) && \
		./configure --prefix=/usr \
			--docdir=/usr/share/doc/flex-2.6.4 \
			--disable-static

$(PACKAGE_DIR)/check-result.out: $(PACKAGE_DIR)/Makefile $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_DIR) check > $@

$(TO_BUILD): $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR)

$(TO_INSTALL): $(PACKAGE_DIR)/check-result.out
	$(MAKE) -j -C $(PACKAGE_DIR) install
	ln -sv flex /usr/bin/lex
