PACKAGE_DIR := findutils-4.8.0
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := find locate updatedb xargs
TO_BUILD := $(addprefix $(BUILD_DIR)/, find/find locate/locate locate/updatedb xargs/xargs)
TO_INSTALL := \
	$(addprefix /usr/bin/, $(filter-out find, $(TARGET))) \
	/bin/find

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr --localstatedir=/var/lib/locate

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

.ONESHELL: $(CHECK_RESULT)
$(CHECK_RESULT): $(TO_BUILD)
	chown -Rv tester $(PACKAGE_DIR)
	su tester -c "PATH=$$PATH $(MAKE) -C $(BUILD_DIR) check" 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
	mv -v /usr/bin/find /bin
	sed -i 's|find:=$${BINDIR}|find:=/bin|' /usr/bin/updatedb
