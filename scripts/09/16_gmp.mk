PACKAGE_DIR := gmp-6.2.1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
TARGET := libgmp.so.10.4.1 libgmpxx.so.4.6.1
TO_BUILD := $(addprefix $(BUILD_DIR)/.libs/, $(TARGET))
TARGET_DOC := gmp.html
DOC_TO_BUILD := $(addprefix $(PACKAGE_DIR)/doc/, $(TARGET_DOC))
DOC_TO_INSTALL := $(addprefix /usr/share/doc/$(PACKAGE_DIR)/, $(TARGET_DOC))
TO_INSTALL := $(addprefix /usr/lib/, $(TARGET))
BUILD_MAKE := $(BUILD_DIR)/Makefile

.PHONY: all clean
all: \
	$(BUILD_DIR)/check-result.out \
	$(TO_INSTALL) \
	$(DOC_TO_INSTALL) \

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR) && \
		./configure --prefix=/usr \
			--enable-cxx \
			--disable-static \
			--docdir=/usr/share/doc/$(PACKAGE_DIR)

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(BUILD_DIR)/check-result.out: $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@
	COUNT=$$(awk '/# PASS:/{total+=$3} ; END{print total}' $@); \
	[ "$COUNT" -eq 197 ] || (echo "failed"; exit 42)

$(TO_INSTALL): $(BUILD_DIR)/check-result.out
	$(MAKE) -C $(BUILD_DIR) -j install

$(DOC_TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j html

$(DOC_TO_INSTALL): $(DOC_TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) -j install-html
