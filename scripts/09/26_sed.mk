PACKAGE_DIR := sed-4.8
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := sed
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := $(addprefix /bin/, $(TARGET))

DOC_TGT := sed.html
DOC_TO_BUILD := $(addprefix $(PACKAGE_DIR)/doc/, $(DOC_TGT))
DOC_TO_INSTALL := $(addprefix /usr/share/doc/$(PACKAGE_DIR)/, $(DOC_TGT))

.PHONY: all setup clean check build install doc
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL) \
	$(DOC_TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

doc: $(DOC_TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr --bindir=/bin

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	chown -Rv tester $(BUILD_DIR)
	su tester -c "PATH=$$PATH $(MAKE) -C $(BUILD_DIR) check" 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install

$(DOC_TO_BUILD):
	$(MAKE) -C $(BUILD_DIR) -j html

$(DOC_TO_INSTALL): $(DOC_TO_BUILD)
	install -d -m755 /usr/share/doc/$(PACKAGE_DIR)
	install -m644 $^ /usr/share/doc/$(PACKAGE_DIR)
