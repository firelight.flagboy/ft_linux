PACKAGE_NAME := iana-etc-20210202
PACKAGE_ARCH := $(PACKAGE_NAME).tar.gz
BUILD_MAKE := $(PACKAGE_NAME)/Makefile
TO_INSTALL := /etc/services /etc/protocols

.PHONY: all clean
all: $(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_NAME): $(PACKAGE_ARCH)
	tar xf $^
	touch $@

/etc/%: | $(PACKAGE_NAME)
	cp $(PACKAGE_NAME)/$* $@
