PACKAGE_DIR := readline-8.1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
TARGET := libreadline.so.8.1 libhistory.so.8.1
TO_BUILD := $(addprefix $(PACKAGE_DIR)/shlib/, $(TARGET))
INSTALL_DIR := /lib
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(TARGET))

.PHONY: all clean
all: $(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)
	cd $(PACKAGE_DIR) && \
		sed -i '/MV.*old/d' Makefile.in && \
		sed -i '/{OLDSUFF}/c:' support/shlib-install

$(PACKAGE_DIR)/Makefile: $(PACKAGE_DIR)/configure
	cd $(PACKAGE_DIR) && \
		./configure --prefix=/usr \
			--disable-static \
			--with-curses \
			--docdir=/usr/share/doc/$(PACKAGE_DIR)

$(TO_BUILD): $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR) SHLIB_LIBS="-lncursesw"

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_DIR) SHLIB_LIBS="-lncursesw" install
	mv -v /usr/lib/lib{readline,history}.so.* /lib
	ln -sfv ../../lib/$$(readlink /usr/lib/libreadline.so) /usr/lib/libreadline.so
	ln -sfv ../../lib/$$(readlink /usr/lib/libhistory.so) /usr/lib/libhistory.so
	cd $(PACKAGE_DIR) && \
		install -v -m644 doc/*.{ps,pdf,html,dvi} /usr/share/doc/$(PACKAGE_DIR)
