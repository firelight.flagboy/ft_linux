PACKAGE_DIR := meson-0.57.1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)

TARGET := meson
TO_BUILD := $(addprefix $(BUILD_DIR)/dest/usr/bin/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

.PHONY: all setup clean build install
all: $(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/setup.py : | $(PACKAGE_DIR)

.ONESHELL: $(TO_BUILD)
$(TO_BUILD): $(PACKAGE_DIR)/setup.py
	cd $(PACKAGE_DIR)
	python3 setup.py build
	python3 setup.py install --root=dest

$(TO_INSTALL): $(TO_BUILD)
	cp -rv $(PACKAGE_DIR)/dest/* /
