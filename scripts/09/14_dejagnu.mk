PACKAGE_DIR := dejagnu-1.6.2
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
TARGET := runtest
DOC_TO_BUILD := $(addprefix $(PACKAGE_DIR)/doc/dejagnu, .html .txt)
TO_BUILD := \
	$(addprefix $(PACKAGE_DIR)/, $(TARGET)) \
	$(DOC_TO_BUILD)

DOC_TO_INST := $(addprefix /usr/share/doc/$(PACKAGE_DIR)/dejagnu, .html .txt)
TO_INSTALL := \
	$(addprefix /usr/bin/, $(TARGET)) \
	$(DOC_TO_INST)

BUILD_MAKE := $(PACKAGE_DIR)/Makefile

.PHONY: all clean
all: \
	$(TO_INSTALL) \
	$(PACKAGE_DIR)/check-result.out

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure
	cd $(PACKAGE_DIR) && \
		./configure --prefix=/usr

$(PACKAGE_DIR)/doc/dejagnu.texi: | $(PACKAGE_DIR)

$(PACKAGE_DIR)/doc/dejagnu.html: $(PACKAGE_DIR)/doc/dejagnu.texi
	cd $(PACKAGE_DIR) && \
		makeinfo --html --no-split -o doc/dejagnu.html doc/dejagnu.texi

$(PACKAGE_DIR)/doc/dejagnu.txt: $(PACKAGE_DIR)/doc/dejagnu.texi
	cd $(PACKAGE_DIR) && \
		makeinfo --plaintext -o doc/dejagnu.txt  doc/dejagnu.texi

$(PACKAGE_DIR)/check-result.out: $(BUILD_MAKE)
	$(MAKE) -C $(PACKAGE_DIR) -j check > $@

$(TO_INSTALL):
	$(MAKE) -C $(PACKAGE_DIR) -j install

/usr/share/doc/$(PACKAGE_DIR):
	install -v -dm755 $@

$(DOC_TO_INST): /usr/share/doc/$(PACKAGE_DIR) $(DOC_TO_BUILD)
	install -v -m644 $(DOC_TO_BUILD) $<
