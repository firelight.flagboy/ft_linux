PACKAGE_DIR := perl-5.32.1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := perl utils/shasum
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, perl shasum)

export BUILD_ZLIB = False
export BUILD_BZIP2 = 0

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/Configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/Configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	sh Configure -des \
		-Dprefix=/usr \
		-Dvendorprefix=/usr \
		-Dprivlib=/usr/lib/perl5/5.32/core_perl \
		-Darchlib=/usr/lib/perl5/5.32/core_perl \
		-Dsitelib=/usr/lib/perl5/5.32/site_perl \
		-Dsitearch=/usr/lib/perl5/5.32/site_perl \
		-Dvendorlib=/usr/lib/perl5/5.32/vendor_perl \
		-Dvendorarch=/usr/lib/perl5/5.32/vendor_perl \
		-Dman1dir=/usr/share/man/man1 \
		-Dman3dir=/usr/share/man/man3 \
		-Dpager="/usr/bin/less -isR" \
		-Duseshrplib \
		-Dusethreads

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) test 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
