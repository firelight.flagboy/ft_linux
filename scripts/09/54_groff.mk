PACKAGE_DIR := groff-1.22.4
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile

TARGET := groff
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

.PHONY: all setup clean build install
all: $(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	PAGE=A4 ./configure --prefix=/usr

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR)

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) -j install
