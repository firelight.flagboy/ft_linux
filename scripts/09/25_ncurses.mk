PACKAGE_DIR := ncurses-6.2
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := \
	lib/libncursesw.so.6.2 \
	progs/clear \

TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := \
	/lib/libncursesw.so.6.2 \
	/usr/bin/clear \

DOC_TO_BUILD := $(PACKAGE_DIR)/doc/html
DOC_TO_INSTALL := /usr/share/doc/$(PACKAGE_DIR)/html

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

doc: $(DOC_TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--mandir=/usr/share/man \
		--with-shared \
		--without-debug \
		--without-normal \
		--enable-pc-files \
		--enable-widec

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@

.ONESHELL: $(TO_INSTALL)
$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install

	# move curses to /lib
	mv -v /usr/lib/libncursesw.so.6* /lib

	# creating link to curses
	ln -sfv ../../lib/$$(readlink /usr/lib/libncursesw.so) /usr/lib/libncursesw.so

	# create non wide char lib
	for lib in ncurses form panel menu ; do
		rm -vf                     /usr/lib/lib$${lib}.so;
		echo "INPUT(-l$${lib}w)" > /usr/lib/lib$${lib}.so;
		ln -sfv $${lib}w.pc        /usr/lib/pkgconfig/$${lib}.pc;
	done

	# create old curses lib file
	rm -vf                     /usr/lib/libcursesw.so
	echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
	ln -sfv libncurses.so      /usr/lib/libcurses.so

	# rm static lib
	rm -fv /usr/lib/libncurses++w.a

$(DOC_TO_INSTALL): $(TO_BUILD)
	mkdir -v       /usr/share/doc/ncurses-6.2
	cp -v -R doc/* /usr/share/doc/ncurses-6.2
