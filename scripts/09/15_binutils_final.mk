PACKAGE_DIR := binutils-2.36.1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)/build-final
TARGET := ld-new
TO_BUILD := $(addprefix $(BUILD_DIR)/ld/.libs/, $(TARGET))
TO_INSTALL := \
	/usr/bin/nm \
	/usr/bin/strip \
	/usr/bin/ld

BUILD_MAKE := $(BUILD_DIR)/Makefile

.PHONY: all clean
all: \
	$(BUILD_DIR)/check-result.out \
	$(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	sed -i '/@\tincremental_copy/d' $(PACKAGE_DIR)/gold/testsuite/Makefile.in

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

$(BUILD_DIR):
	mkdir -p $@

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR) && \
		../configure --prefix=/usr \
			--enable-gold \
			--enable-ld=default \
			--enable-plugins \
			--enable-shared \
			--disable-werror \
			--enable-64-bit-bfd \
			--with-system-zlib

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j tooldir=/usr

$(BUILD_DIR)/check-result.out: $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -k check > $@

$(TO_INSTALL): $(BUILD_DIR)/check-result.out
	$(MAKE) -C $(BUILD_DIR) -j tooldir=/usr install
	$(RM) -fv /usr/lib/lib{bfd,ctf,ctf-nobfd,opcodes}.a
