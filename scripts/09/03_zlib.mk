PACKAGE_DIR := zlib-1.2.11
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
TARGET := libz.so.1.2.11
TO_BUILD := $(addprefix $(PACKAGE_DIR)/, $(TARGET))
INSTALL_DIR := /lib
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(TARGET))

.PHONY: all patch clean
all: $(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $^
	touch $@

$(PACKAGE_DIR)/configure: | $(PACKAGE_DIR)

$(BUILD_DIR): | $(PACKAGE_DIR)
	mkdir -p $@

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	../configure --prefix=/usr

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -j -C $(BUILD_DIR)

$(BUILD_DIR)/check-result.out: $(TO_BUILD)
	$(MAKE) -j -C $(BUILD_DIR) check > $@

$(TO_INSTALL): $(BUILD_DIR)/check-result.out
	$(MAKE) -j -C $(BUILD_DIR) install
	mv -v /usr/lib/libz.so.* /lib
	ln -sfv ../../lib/$$(readlink /usr/lib/libz.so) /usr/lib/libz.so
	rm -fv /usr/lib/libz.a
