PACKAGE_DIR := XML-Parser-2.46
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := Expat.so
TO_BUILD := $(addprefix $(BUILD_DIR)/blib/arch/auto/XML/Parser/Expat/, $(TARGET))
TO_INSTALL := $(addprefix /usr/lib/perl5/5.32/site_perl/auto/XML/Parser/Expat/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/Makefile.PL : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/Makefile.PL | $(BUILD_DIR)
	cd $(BUILD_DIR)
	perl Makefile.PL

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) test 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
