PACKAGE_NAME := procps-ng-3.3.17
PACKAGE_DIR := procps-3.3.17
PACKAGE_ARCH := $(PACKAGE_NAME).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := free pgrep pkill pidof ps top uptime watch
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(filter-out top, $(TARGET)) top/top)
TO_INSTALL := $(addprefix /bin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--exec-prefix= \
		--libdir=/usr/lib \
		--docdir=/usr/share/doc/$(PACKAGE_NAME) \
		--disable-static \
		--disable-kill \
		--with-systemd

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install

	mv -v /usr/lib/libprocps.so.* /lib
	ln -sfv ../../lib/$$(readlink /usr/lib/libprocps.so) /usr/lib/libprocps.so
