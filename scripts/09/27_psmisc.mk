PACKAGE_DIR := psmisc-23.4
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile

TARGET := fuser killall peekfd prtstat pslog pstree
TO_BUILD := $(addprefix $(BUILD_DIR)/src/, $(TARGET))
TO_INSTALL := \
	$(addprefix /usr/bin/, peekfd prtstat pslog pstree) \
	$(addprefix /bin/, fuser killall) \

.PHONY: all setup clean check build install
all: $(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) -j install
	mv -v /usr/bin/{fuser,killall} /bin
