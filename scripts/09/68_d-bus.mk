PACKAGE_DIR := dbus-1.12.20
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile

TARGET := dbus-launch dbus-uuidgen
TO_BUILD := $(addprefix $(BUILD_DIR)/, \
		bus/dbus-daemon \
		tools/dbus-launch \
		tools/dbus-uuidgen \
	)
TO_INSTALL := \
	$(addprefix /usr/bin/, $(TARGET)) \
	/var/lib/dbus

.PHONY: all setup clean build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--disable-static \
		--disable-doxygen-docs \
		--disable-xml-docs \
		--docdir=/usr/share/doc/$(PACKAGE_DIR) \
		--with-console-auth-dir=/run/console \
		--with-system-pid-file=/run/dbus/pid \
		--with-system-socket=/run/dbus/system_bus_socket

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) -j install

	# the shared lib needs to be moved to `/lib`
	mv -v /usr/lib/libdbus-1.so.* /lib
	ln -sfv ../../lib/$$(readlink /usr/lib/libdbus-1.so) /usr/lib/libdbus-1.so

	# create symlink for systemd & dbus
	ln -sfv /etc/machine-id /var/lib/dbus
