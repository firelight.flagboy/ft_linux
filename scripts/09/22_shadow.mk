PACKAGE_DIR := shadow-4.8.1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := \
	chpasswd \
	chfn \
	chsh \
	expiry \
	gpasswd \
	grpconv \
	groupdel \
	id \
	logoutd \
	nologin \
	newusers \
	passwd \
	pwconv \
	pwunconv \
	userdel \
	usermod \

TO_BUILD := $(addprefix $(BUILD_DIR)/src/, $(TARGET))
TO_INSTALL := \
	/usr/sbin/pwck \
	/usr/sbin/useradd \
	/sbin/nologin \
	/usr/bin/gpasswd \
	/usr/sbin/grpconv \
	/usr/sbin/userdel \
	/usr/sbin/usermod \
	/usr/bin/passwd \
	/usr/sbin/groupdel \
	/usr/sbin/logoutd \
	/bin/login \
	/usr/bin/chage \
	/usr/bin/newgrp \
	/usr/bin/faillog \
	/usr/sbin/grpunconv \
	/usr/sbin/chpasswd \
	/usr/sbin/pwunconv \
	/usr/sbin/newusers \
	/usr/sbin/groupmod \
	/usr/sbin/groupadd \
	/usr/sbin/vipw \
	/usr/bin/lastlog \
	/usr/sbin/grpck \
	/usr/sbin/groupmems \
	/usr/sbin/chgpasswd \
	/bin/su \
	/usr/bin/expiry \
	/usr/sbin/pwconv \
	/usr/bin/chfn \
	/usr/bin/chsh \
	/usr/bin/newuidmap \
	/usr/bin/newgidmap \

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

.ONESHELL: $(PACKAGE_DIR)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	cd $@

	# disable the installation of the `groups' program and its man pages, coreutils will install that
	sed -i 's/groups$$(EXEEXT) //' src/Makefile.in
	find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
	find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
	find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;

	# use sha512 for passwd encryption
	sed -e 's:#ENCRYPT_METHOD DES:ENCRYPT_METHOD SHA512:' \
		-e 's:/var/spool/mail:/var/mail:' \
		-i etc/login.defs

	# first user start at 1000
	sed -i 's/1000/999/' etc/useradd

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	touch /usr/bin/passwd
	./configure --sysconfdir=/etc \
		--with-group-name-max-length=32

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) -j install

	# enable shadowing for passwd & group passwd
	pwconv
	grpconv

	# disable mailspool creation
	sed -i 's/yes/no/' /etc/default/useradd
