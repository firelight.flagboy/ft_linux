PACKAGE_DIR := file-5.39
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
TARGET := file
TO_BUILD := $(addprefix $(PACKAGE_DIR)/src/.libs/, $(TARGET))
INSTALL_DIR := /usr/bin
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(TARGET))

.PHONY: all clean
all: \
	$(PACKAGE_DIR)/check-result.out \
	$(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)
$(PACKAGE_DIR)/Makefile: $(PACKAGE_DIR)/configure
	cd $(PACKAGE_DIR) && ./configure --prefix=/usr

$(PACKAGE_DIR)/check-result.out: $(TO_BUILD) $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR) check > $@

$(TO_BUILD): $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR)

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_DIR) install
