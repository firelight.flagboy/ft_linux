PACKAGE_DIR := zstd-1.4.8
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
TARGET := zstd
TO_BUILD := $(addprefix $(PACKAGE_DIR)/programs/, $(TARGET))
INSTALL_DIR := /usr/bin
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(TARGET))

.PHONY: all clean
all: \
	$(PACKAGE_DIR)/check-result.out \
	$(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/Makefile: | $(PACKAGE_DIR)

$(PACKAGE_DIR)/check-result.out: $(TO_BUILD) $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR) check > $@

$(TO_BUILD): $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR)

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_DIR) install
	rm -v /usr/lib/libzstd.a
	mv -v /usr/lib/libzstd.so.* /lib
	ln -sfv ../../lib/$$(readlink /usr/lib/libzstd.so) /usr/lib/libzstd.so
