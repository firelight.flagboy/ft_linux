PACKAGE_DIR := mpc-1.2.1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile

TARGET := libmpc.so.3.2.1
TO_BUILD := $(addprefix $(BUILD_DIR)/src/.libs, $(TARGET))
TO_INSTALL := $(addprefix /usr/lib/, $(TARGET))

DOC_TARGET := mpc.html
DOC_TO_BUILD := $(addprefix $(BUILD_DIR)/doc/, $(DOC_TARGET))
DOC_TO_INSTALL := $(addprefix /usr/share/doc/$(PACKAGE_DIR)/, $(DOC_TARGET))

.PHONY: all clean
all: \
	$(BUILD_DIR)/check-result.out \
	$(TO_INSTALL) \
	$(DOC_TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR) && \
		./configure --prefix=/usr \
			--disable-static \
			--docdir=/usr/share/doc/$(PACKAGE_DIR)

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(BUILD_DIR)/check-result.out: $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@

$(TO_INSTALL): $(BUILD_DIR)/check-result.out
	$(MAKE) -C $(BUILD_DIR) -j install

$(DOC_TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j html

$(DOC_TO_INSTALL): $(DOC_TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) -j install-html
