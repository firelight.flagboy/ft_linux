PACKAGE_DIR := elfutils-0.183
PACKAGE_ARCH := $(PACKAGE_DIR).tar.bz2
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := libelf-0.183.so
TO_BUILD := $(BUILD_DIR)/libelf/libelf.so
TO_INSTALL := $(addprefix /lib/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--disable-debuginfod \
		--enable-libdebuginfod=dummy \
		--libdir=/lib

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	# install only libelf
	$(MAKE) -C $(BUILD_DIR)/libelf -j install
	install -vm644 config/libelf.pc /usr/lib/pkgconfig
	rm /lib/libelf.a
