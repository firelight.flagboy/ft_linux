.PHONY: all clean
all: /lib/libc-2.33.so

clean:
	$(RM) -rf $(PACKAGE_DIR)

/lib/libc-2.33.so: /sources/09_scripts/02_glib_final.sh
	bash $<
