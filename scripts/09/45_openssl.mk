PACKAGE_DIR := openssl-1.1.1j
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := openssl
TO_BUILD := $(addprefix $(BUILD_DIR)/apps/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

.PHONY: all setup clean check build install doc
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

doc:
	cp -vfr doc/* /usr/share/doc/openssl-1.1.1j

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/config : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/config | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./config --prefix=/usr \
		--openssldir=/etc/ssl \
		--libdir=lib \
		shared \
		zlib-dynamic

$(TO_BUILD): | $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	# test 30-test_afalg.t is known to fail on some kernel config
	$(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j MANSUFFIX=ssl install
	mv -v /usr/share/doc/openssl /usr/share/doc/openssl-1.1.1j
