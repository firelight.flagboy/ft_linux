PACKAGE_DIR := bzip2-1.0.8
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_MAKE := $(PACKAGE_DIR)/Makefile
LIB_MAKE := $(PACKAGE_DIR)/Makefile-libbz2_so
LIB := libbz2.so.1.0.8
TO_LIB := $(addprefix $(PACKAGE_DIR)/, $(LIB))
TARGET := bzip2 bzip2-shared
TO_BUILD := $(addprefix $(PACKAGE_DIR)/, $(TARGET))
INSTALL_DIR := /usr
TO_INSTALL := \
	/bin/bzip2 \
	/lib/libbz2.so.1.0.8 \
	/bin/bunzip2 \
	/bin/bzcat

.PHONY: all clean
all: $(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

.ONESHELL: $(PACKAGE_DIR)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $^
	touch $@
	cd $@
	patch -Np1 -i ../$(PACKAGE_DIR)-install_docs-1.patch
	sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
	sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile

$(LIB_MAKE): | $(PACKAGE_DIR)

$(TO_LIB): $(LIB_MAKE)
	$(MAKE) -j -C $(PACKAGE_DIR) -f $(LIB_MAKE)

$(TO_BUILD): $(TO_LIB)
	$(MAKE) -j -C $(PACKAGE_DIR)

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_DIR) PREFIX=/usr install
	cp -v $(PACKAGE_DIR)/bzip2-shared /bin/bzip2
	cp -va $(PACKAGE_DIR)/libbz2.so* /lib
	touch /lib/libbz2.so*
	ln -sf ../../lib/libbz2.so.1.0 /usr/lib/libbz2.so
	rm -v /usr/bin/{bunzip2,bzcat,bzip2}
	ln -sf bzip2 /bin/bunzip2
	ln -sf bzip2 /bin/bzcat
	rm -fv /usr/lib/libbz2.a
