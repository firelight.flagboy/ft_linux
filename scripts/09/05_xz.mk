PACKAGE_DIR := xz-5.2.5
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
TARGET := lzcat unxz xzcat xz lzma
TO_BUILD := \
	$(PACKAGE_DIR)/src/xz/xz
INSTALL_DIR := /bin
TO_INSTALL := $(addprefix $(INSTALL_DIR)/, $(TARGET))

.PHONY: all clean
all: \
	$(PACKAGE_DIR)/check-result.out \
	$(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<

$(PACKAGE_DIR)/configure: | $(PACKAGE_DIR)

$(PACKAGE_DIR)/Makefile: $(PACKAGE_DIR)/configure
	cd $(PACKAGE_DIR) && \
	./configure --prefix=/usr --disable-static --docdir=/usr/share/doc/$(PACKAGE_DIR)

$(PACKAGE_DIR)/check-result.out: $(TO_BUILD) $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR) check > $@

$(TO_BUILD): $(PACKAGE_DIR)/Makefile
	$(MAKE) -j -C $(PACKAGE_DIR)

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -j -C $(PACKAGE_DIR) install
	mv -v $(addprefix /usr/bin/, $(TARGET)) /bin
	mv -v /usr/lib/liblzma.so.* /lib
	ln -svf ../../lib/$$(readlink /usr/lib/liblzma.so) /usr/lib/liblzma.so
