PACKAGE_DIR := bash-5.1
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := bash
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := $(addprefix /bin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

.ONESHELL: $(PACKAGE_DIR)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	cd $(PACKAGE_DIR)
	sed -i.orig '/^bashline.o:.*shmbchar.h/a bashline.o: $${DEFDIR}/builtext.h' Makefile.in
	diff Makefile.in Makefile.in.orig || true

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--docdir=/usr/share/doc/$(PACKAGE_DIR) \
		--without-bash-malloc \
		--with-installed-readline

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

.ONESHELL: $(CHECK_RESULT)
$(CHECK_RESULT): $(TO_BUILD)
	chown -Rv tester $(BUILD_DIR)
	(
		su tester << EOF
		PATH=$$PATH $(MAKE) -C $(BUILD_DIR) tests < $$(tty)
		EOF
	) 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
	mv -fv /usr/bin/bash /bin
