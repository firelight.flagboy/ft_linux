PACKAGE_DIR := coreutils-8.32
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

STD_TARGET := \
	cat \
	chgrp \
	chmod \
	chown \
	cp \
	date \
	dd \
	df \
	echo \
	false \
	head \
	ln \
	ls \
	mkdir \
	mknod \
	mv \
	nice \
	pwd \
	rm \
	rmdir \
	stty \
	sleep \
	sync \
	true \
	touch \
	uname

ADM_TARGET := chroot

OTH_TARGET := \
	[ \
	b2sum \
	base32 \
	base64 \
	basename \
	basenc \
	chcon \
	cksum \
	comm \
	csplit \
	cut \
	dir \
	dircolors \
	dirname \
	du \
	env \
	expand \
	expr \
	factor \
	fmt \
	fold \
	groups \
	hostid \
	id \
	install \
	join \
	link \
	logname \
	md5sum \
	mkfifo \
	mktemp \
	nl \
	nohup \
	nproc \
	numfmt \
	od \
	paste \
	pathchk \
	pinky \
	pr \
	printenv \
	printf \
	ptx \
	readlink \
	realpath \
	runcon \
	seq \
	sha1sum \
	sha224sum \
	sha256sum \
	sha384sum \
	sha512sum \
	shred \
	shuf \
	sort \
	split \
	stat \
	stdbuf \
	sum \
	tac \
	tail \
	tee \
	test \
	timeout \
	tr \
	truncate \
	tsort \
	tty \
	unexpand \
	uniq \
	unlink \
	users \
	vdir \
	wc \
	who \
	whoami \
	yes

TARGET := \
	$(STD_TARGET) \
	$(ADM_TARGET) \
	$(OTH_TARGET)

TO_BUILD := \
	$(addprefix $(BUILD_DIR)/src/, $(filter-out install, $(TARGET))) \

TO_INSTALL := \
	$(addprefix /usr/bin/, $(OTH_TARGET)) \
	$(addprefix /bin/, $(STD_TARGET)) \
	$(addprefix /usr/sbin/, $(ADM_TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

print-%:
	@echo $($*) | tr ' ' '\n' | xargs -L 1 echo

.ONESHELL: $(PACKAGE_DIR)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	cd $@
	patch -Np1 -i ../coreutils-8.32-i18n-1.patch

	# remove test that may loop forever
	sed -i '/test.lock/s/^/#/' gnulib-tests/gnulib.mk

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)
	cd $(BUILD_DIR) && \
		autoreconf -fiv

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	FORCE_UNSAFE_CONFIGURE=1 ./configure \
		--prefix=/usr \
		--enable-no-install-program=kill,uptime

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	(
		set -x
		$(MAKE) -C $(BUILD_DIR) NON_ROOT_USERNAME=tester check-root

		# create & add `dummy` group to tester
		groupadd -f -g 102 dummy
		usermod -aG dummy tester

		cd $(BUILD_DIR)
		chown -Rv tester .

		# test-getlogin is known to fail
		su tester -c "PATH=$$PATH make RUN_EXPENSIVE_TESTS=yes check"

		# remove `dummy` group
		sed -i '/dummy/d' /etc/group
	) 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install

	mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} /bin
	mv -v /usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} /bin
	mv -v /usr/bin/{rmdir,stty,sync,true,uname} /bin
	mv -v /usr/bin/{head,nice,sleep,touch} /bin

	mv -v /usr/bin/chroot /usr/sbin

	mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
	sed -i 's/"1"/"8"/' /usr/share/man/man8/chroot.8
