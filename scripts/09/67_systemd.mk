PACKAGE_DIR := systemd-247
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)/build
BUILD_MAKE := $(BUILD_DIR)/build.ninja

TARGET := systemctl
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := \
	/bin/systemctl

.PHONY: all setup clean build install
all: \
	$(TO_INSTALL) \
	/etc/machine-id

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

build: $(TO_BUILD)

install: $(TO_INSTALL)

.ONESHELL: $(PACKAGE_DIR)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	cd $@

	patch -Np1 -i ../$(PACKAGE_DIR)-upstream_fixes-1.patch

	# work around `xsltproc` not being installed
	ln -sf /bin/true /usr/bin/xsltproc

	# remove tests that cannot be built in chroot
	sed '181,$$ d' -i src/resolve/meson.build

	# remove unneeded group `render`
	sed -i 's/GROUP="render"/GROUP="video"/' rules.d/50-udev-default.rules.in


.ONESHELL: $(PACKAGE_DIR)/systemd-man-pages-247
$(PACKAGE_DIR)/systemd-man-pages-247:
	cd $(PACKAGE_DIR)
	tar -xf ../systemd-man-pages-247.tar.xz

$(PACKAGE_DIR)/meson.build : | $(PACKAGE_DIR)

$(BUILD_DIR): | $(PACKAGE_DIR)
	mkdir -p $@

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/meson.build | $(BUILD_DIR)
	cd $(BUILD_DIR)
	LANG=en_US.UTF-8 \
	meson --prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		-Dblkid=true \
		-Dbuildtype=release \
		-Ddefault-dnssec=no \
		-Dfirstboot=false \
		-Dinstall-tests=false \
		-Dkmod-path=/bin/kmod \
		-Dldconfig=false \
		-Dmount-path=/bin/mount \
		-Drootprefix= \
		-Drootlibdir=/lib \
		-Dsplit-usr=true \
		-Dsulogin-path=/sbin/sulogin \
		-Dsysusers=false \
		-Dumount-path=/bin/umount \
		-Db_lto=false \
		-Drpmmacrosdir=no \
		-Dhomed=false \
		-Duserdb=false \
		-Dman=true \
		-Dmode=release \
		-Ddocdir=/usr/share/doc/$(PACKAGE_DIR) \
		..

$(TO_BUILD): $(BUILD_MAKE)
	cd $(BUILD_DIR) && \
		LANG=en_US.UTF-8 ninja

$(TO_INSTALL): $(TO_BUILD)
	cd $(BUILD_DIR) && \
		LANG=en_US.UTF-8 ninja install

	# rm filesystem
	rm -f /usr/bin/xsltproc
	rm -rf /usr/lib/pam.d

	systemd-machine-id-setup

	systemctl preset-all
	systemctl disable systemd-time-wait-sync.service

/etc/machine-id:
	systemd-machine-id-setup
