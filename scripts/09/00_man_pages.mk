PACKAGE_NAME := man-pages-5.10
PACKAGE_ARCH := $(PACKAGE_NAME).tar.xz
BUILD_MAKE := $(PACKAGE_NAME)/Makefile
TO_INSTALL := /usr/share/man/man1

.PHONY: all clean
all: $(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_NAME): $(PACKAGE_ARCH)
	tar xf $^
	touch $@

$(PACKAGE_NAME)/Makefile: | $(PACKAGE_NAME)

$(TO_INSTALL): $(PACKAGE_NAME)/Makefile
	$(MAKE) -j -C $(PACKAGE_NAME) install
