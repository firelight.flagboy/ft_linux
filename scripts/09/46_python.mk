PACKAGE_DIR := Python-3.9.2
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := python
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := /usr/bin/python3

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

doc:
	install -v -dm755 /usr/share/doc/$(PACKAGE_DIR)/html

	tar --strip-components=1  \
		--no-same-owner       \
		--no-same-permissions \
		-C /usr/share/doc/$(PACKAGE_DIR)/html \
		-xvf ../$(PACKAGE_DIR)-docs-html.tar.bz2

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--enable-shared \
		--with-system-expat \
		--with-system-ffi \
		--with-ensurepip=yes \
		--includedir=/usr/include \
		--includedir=/usr/include/$$(uname -m)-linux-gnu

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	# The test named test_normalization is know to fail
	$(MAKE) -C $(BUILD_DIR) test 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
