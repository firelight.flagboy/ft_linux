PACKAGE_DIR := ninja-1.10.2
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := ninja
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure.py : | $(PACKAGE_DIR)

$(TO_BUILD): $(PACKAGE_DIR)/configure.py
	cd $(PACKAGE_DIR)
	python3 configure.py --bootstrap

.ONESHELL: $(CHECK_RESULT)
$(CHECK_RESULT): $(TO_BUILD)
	(
		cd $(BUILD_DIR)
		./ninja ninja_test
		./ninja_test --gtest_filter=-SubprocessTest.SetWithLots
	) 2>&1 | tee $@

.ONESHELL: $(TO_INSTALL)
$(TO_INSTALL): $(CHECK_RESULT)
	cd $(BUILD_DIR)
	install -vm755 ninja /usr/bin/
	install -vDm644 misc/bash-completion /usr/share/bash-completion/completions/ninja
	install -vDm644 misc/zsh-completion  /usr/share/zsh/site-functions/_ninja
