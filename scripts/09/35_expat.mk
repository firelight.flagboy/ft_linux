PACKAGE_DIR := expat-2.2.10
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := xmlwf
TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

DOCS := \
	expat.png \
	reference.html \
	style.css \
	valid-xhtml10.png \
	xmlwf.1 \
	xmlwf.xml
DOC_REQ := $(addprefix $(PACKAGE_DIR)/doc/, $(DOCS))
DOC_TO_INSTALL := $(addprefix /usr/share/doc/$(PACKAGE_DIR)/, $(DOCS))

.PHONY: all setup clean check build install doc
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

doc: $(DOC_TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--disable-static \
		--docdir=/usr/share/doc/$(PACKAGE_DIR)

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install

$(DOC_TO_INSTALL): $(DOC_REQ)
	install -v -m644 doc/*.{html,png,css} /usr/share/doc/$(PACKAGE_DIR)
