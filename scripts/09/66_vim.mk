PACKAGE_DIR := vim-8.2.2433
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := vim vimtutor
TO_BUILD := $(addprefix $(BUILD_DIR)/src/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET))

.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL) \
	/etc/vimrc

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

.ONESHELL: /etc/vimrc
/etc/vimrc:
	cat > /etc/vimrc << "EOF"
	" Begin /etc/vimrc

	" Ensure defaults are set before customizing settings, not after
	source $$VIMRUNTIME/defaults.vim
	let skip_defaults_vim=1

	set nocompatible
	set backspace=2
	set mouse=
	syntax on
	if (&term == "xterm") || (&term == "putty")
	set background=dark
	endif

	" End /etc/vimrc
	EOF

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

	echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> $(PACKAGE_DIR)/src/feature.h

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	chown -Rv tester $(BUILD_DIR)
	su tester -c "LANG=en_US.UTF-8 $(MAKE) -j1 -C $(BUILD_DIR) test" 2>&1 > $@

.ONESHELL: $(TO_INSTALL)
$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
	ln -sv vim /usr/bin/vi
	for L in  /usr/share/man/{,*/}man1/vim.1; do
		ln -sv vim.1 $$(dirname $$L)/vi.1
	done
	ln -sv ../vim/vim82/doc /usr/share/doc/$(PACKAGE_DIR)
