PACKAGE_DIR := gcc-10.2.0
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)/build-final
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := \
	$(addprefix gcc-, ranlib ar nm)

TO_BUILD := $(addprefix $(BUILD_DIR)/gcc/, $(TARGET))
TO_INSTALL := $(addprefix /usr/bin/, $(TARGET) gcc)

SANITY_RESULT := $(BUILD_DIR)/sanity-result.out

.PHONY: all setup clean check build install sanity
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL) \
	$(SANITY_RESULT)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

sanity: $(SANITY_RESULT)

.ONESHELL: $(PACKAGE_DIR)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	cd $@
	case $$(uname -m) in
		x86_64)
			sed -e '/m64=/s/lib64/lib/' \
				-i.orig gcc/config/i386/t-linux64
		;;
	esac

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

$(BUILD_DIR): | $(PACKAGE_DIR)
	mkdir -p $@

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	../configure --prefix=/usr \
		LD=ld \
		--enable-languages=c,c++ \
		--disable-multilib \
		--disable-bootstrap \
		--with-system-zlib

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR)

.ONESHELL: $(CHECK_RESULT)
$(CHECK_RESULT): $(TO_BUILD)
	ulimit -s 32768
	chown -Rv tester $(BUILD_DIR)
	cd $(BUILD_DIR)
	su tester -c "PATH=$$PATH $(MAKE) -C $(BUILD_DIR) -k check" 2>&1 | tee $@
	../contrib/test_summary | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) install

	# remove unneeded dir
	$(RM) -rf /usr/lib/gcc/$$(gcc -dumpmachine)/10.2.0/include-fixed/bits/

	# change ownershipt to root
	chown -v -R root:root \
		/usr/lib/gcc/*linux-gnu/10.2.0/include{,-fixed}

	# symlink required by the FHS
	ln -sfv ../usr/bin/cpp /lib

	# compatibility symlink to enable Link Time Optimization (LTO)
	ln -sfv ../../libexec/gcc/$$(gcc -dumpmachine)/10.2.0/liblto_plugin.so \
		/usr/lib/bfd-plugins/

	# move misplaced file
	mkdir -pv /usr/share/gdb/auto-load/usr/lib
	mv -v /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib

.ONESHELL: $(SANITY_RESULT)
$(SANITY_RESULT): $(TO_INSTALL)
	(
		set -x
		echo 'int main(){}' > dummy.c
		cc dummy.c -v -Wl,--verbose &> dummy.log

		# expect to see:
		#   [Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
		readelf -l a.out | grep ': /lib'

		# expect to see:
		#   /usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/../../../../lib/crt1.o succeeded
		#   /usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/../../../../lib/crti.o succeeded
		#   /usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/../../../../lib/crtn.o succeeded
		grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log;

		# verify that the compiler is searching for the correct header
		# expect to see:
		#   #include <...> search starts here:
		#   /usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/include
		#   /usr/local/include
		#   /usr/lib/gcc/x86_64-pc-linux-gnu/10.2.0/include-fixed
		#   /usr/include
		grep -B4 '^ /usr/include' dummy.log

		# verify that the new linker is used with the correct path
		# expected to see:
		#   SEARCH_DIR("/usr/x86_64-pc-linux-gnu/lib64")
		#   SEARCH_DIR("/usr/local/lib64")
		#   SEARCH_DIR("/lib64")
		#   SEARCH_DIR("/usr/lib64")
		#   SEARCH_DIR("/usr/x86_64-pc-linux-gnu/lib")
		#   SEARCH_DIR("/usr/local/lib")
		#   SEARCH_DIR("/lib")
		#   SEARCH_DIR("/usr/lib");
		grep 'SEARCH.*/usr/lib' dummy.log |sed 's|; |\n|g'

		# check we use the correct libc
		# expect to see:
		#   attempt to open /lib/libc.so.6 succeeded
		grep "/lib.*/libc.so.6 " dummy.log

		# gcc is using the correct dynamic linker
		# expect to see:
		#   found ld-linux-x86-64.so.2 at /lib/ld-linux-x86-64.so.2
		grep found dummy.log
		rm -v dummy.c a.out dummy.log
	) 2>&1 | tee $(BUILD_DIR)/sanity-check.out
