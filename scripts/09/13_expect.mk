PACKAGE_DIR := expect5.45.4
PACKAGE_ARCH := $(PACKAGE_DIR).tar.gz
TARGET := expect
TO_BUILD := $(addprefix $(PACKAGE_DIR)/, $(TARGET))
TO_INSTALL := \
	$(addprefix /usr/bin/, $(TARGET)) \
	/usr/lib/libexpect5.45.4.so

BUILD_MAKE := $(PACKAGE_DIR)/Makefile

.PHONY: all clean
all: $(TO_INSTALL)

clean:
	$(RM) -rf $(PACKAGE_DIR)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure
	cd $(PACKAGE_DIR) && \
		./configure --prefix=/usr \
			--with-tcl=/usr/lib \
			--enable-shared \
			--mandir=/usr/share/man \
			--with-tclinclude=/usr/include

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(PACKAGE_DIR) -j

$(PACKAGE_DIR)/check-result.out: $(BUILD_MAKE) $(TO_BUILD)
	$(MAKE) -C $(PACKAGE_DIR) -j test > $@

$(TO_INSTALL): $(TO_BUILD) $(PACKAGE_DIR)/check-result.out
	$(MAKE) -C $(PACKAGE_DIR) -j install
	ln -svf expect5.45.4/libexpect5.45.4.so /usr/lib
