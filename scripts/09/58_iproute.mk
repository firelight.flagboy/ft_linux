PACKAGE_DIR := iproute2-5.10.0
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile

TARGET := ip lnstat
TO_BUILD := $(addprefix $(BUILD_DIR)/, ip/ip misc/lnstat)
TO_INSTALL := $(addprefix /sbin/, $(TARGET))

.PHONY: all setup clean build install
all: $(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

build: $(TO_BUILD)

install: $(TO_INSTALL)

.ONESHELL: $(PACKAGE_DIR)
$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@
	cd $@

	# disable arpd
	sed -i /ARPD/d Makefile
	rm -fv man/man8/arpd.8

	# disable module that require iptables
	sed -i 's/.m_ipt.o//' tc/Makefile

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): | $(BUILD_DIR)

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(TO_INSTALL): $(TO_BUILD)
	$(MAKE) -C $(BUILD_DIR) -j DOCDIR=/usr/share/doc/$(PACKAGE_DIR) install
