PACKAGE_DIR := inetutils-2.0
PACKAGE_ARCH := $(PACKAGE_DIR).tar.xz
BUILD_DIR := $(PACKAGE_DIR)
BUILD_MAKE := $(BUILD_DIR)/Makefile
CHECK_RESULT := $(BUILD_DIR)/check-result.out

TARGET := \
	src/dnsdomainname \
	ftp/ftp \
	src/tftp \
	ifconfig/ifconfig \
	src/hostname \
	ping/ping \
	ping/ping6 \
	talk/talk \
	telnet/telnet \
	src/traceroute

TO_BUILD := $(addprefix $(BUILD_DIR)/, $(TARGET))
TO_INSTALL := \
	$(addprefix /usr/bin/, dnsdomainname ftp talk telnet tftp) \
	$(shell echo /bin/{hostname,ping,ping6,traceroute}) \
	/sbin/ifconfig


.PHONY: all setup clean check build install
all: \
	$(CHECK_RESULT) \
	$(TO_INSTALL)

setup: $(BUILD_MAKE)

clean:
	$(RM) -rf $(PACKAGE_DIR)

check: $(CHECK_RESULT)

build: $(TO_BUILD)

install: $(TO_INSTALL)

$(PACKAGE_DIR): $(PACKAGE_ARCH)
	tar xf $<
	touch $@

$(PACKAGE_DIR)/configure : | $(PACKAGE_DIR)

.ONESHELL: $(BUILD_MAKE)
$(BUILD_MAKE): $(PACKAGE_DIR)/configure | $(BUILD_DIR)
	cd $(BUILD_DIR)
	./configure --prefix=/usr \
		--localstatedir=/var \
		--disable-logger \
		--disable-whois \
		--disable-rcp \
		--disable-rexec \
		--disable-rlogin \
		--disable-rsh \
		--disable-servers

$(TO_BUILD): $(BUILD_MAKE)
	$(MAKE) -C $(BUILD_DIR) -j

$(CHECK_RESULT): $(TO_BUILD)
	# `libls.sh` may fail in chroot
	# `ping-localhost.sh` will fail if host does not have ipv6 caps
	$(MAKE) -C $(BUILD_DIR) check 2>&1 | tee $@

$(TO_INSTALL): $(CHECK_RESULT)
	$(MAKE) -C $(BUILD_DIR) -j install
	mv -v /usr/bin/{hostname,ping,ping6,traceroute} /bin
	mv -v /usr/bin/ifconfig /sbin
