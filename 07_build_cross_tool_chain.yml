---
- name: build m4
  tags: [m4]
  hosts: host
  become: true
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: m4
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"


    - name: apply fixes from glibc-2.28
      block:
        - name: find all files to fix
          shell: |
            grep -Rl IO_ftrylockfile *.c
          args:
            chdir: "{{ package_dir }}/lib"
          changed_when: false
          failed_when: files_to_be_fixed.stderr|length > 0
          register: files_to_be_fixed

        - name: apply fix io eof
          replace:
            replace: IO_EOF_SEEN
            regexp: IO_ftrylockfile
            path: "{{ (package_dir, 'lib', item) | path_join }}"
          loop: "{{ files_to_be_fixed.stdout_lines }}"

        - name: apply fix io backup
          lineinfile:
            path: "{{ package_dir }}/lib/stdio-impl.h"
            line: "#define _IO_IN_BACKUP 0x100"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
            --host={{ lfs_target_name }} \
            --build=$(build-aux/config.guess)
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      shell: |
        make -j
      args:
        creates: "{{ package_dir }}/lib/libm4.a"
        chdir: "{{ package_dir }}"

    - name: install
      shell: |
        make -j DESTDIR={{ lfs_main_dir }} install
      args:
        creates: "{{ lfs_main_dir }}/usr/bin/m4"
        chdir: "{{ package_dir }}"

- name: build ncurses
  tags: [ncurses]
  hosts: host
  become: true
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: ncurses
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: ensure gawk is found
      replace:
        path: "{{ package_dir }}/configure"
        regexp: mawk

    - name: create build dir
      file:
        state: directory
        path: "{{ package_dir }}/build"

    - name: configure makefile
      command: ../configure
      args:
        chdir: "{{ package_dir }}/build"
        creates: "{{ package_dir }}/build/Makefile"

    - name: execute alt makefile
      community.general.make:
        chdir: "{{ item.chdir }}"
        jobs: "{{ ansible_processor_cores }}"
        target: "{{ item.target | default('all') }}"
      loop:
        - chdir: "{{ package_dir }}/build/include"
        - chdir: "{{ package_dir }}/build/progs"
          target: tic

    - name: configre makefile
      shell: |
        ./configure --prefix=/usr \
          --host={{ lfs_target_name }} \
          --build=$(./config.guess) \
          --mandir=/usr/share/man \
          --with-manpage-format=normal \
          --with-shared \
          --without-debug \
          --without-ada \
          --without-normal \
          --enable-widec
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: execute main makefile
      community.general.make:
        chdir: "{{ package_dir }}"
        jobs: "{{ ansible_processor_cores }}"

    - name: install ncurses
      shell: make -j DESTDIR={{ lfs_main_dir }} TIC_PATH={{ package_dir }}/build/progs/tic install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ (lfs_main_dir, 'usr/lib', 'libncursesw.so.{}'.format(package.version)) | path_join }}"

    - name: create pseudo libncurse.so
      copy:
        content: "INPUT(-lncursesw)"
        dest: "{{ lfs_main_dir }}/usr/lib/libncurses.so"

    - name: copy lib file to /lib
      shell: |
        cp {{ lfs_main_dir }}/usr/lib/libncursesw.so.6* {{ lfs_main_dir }}/lib
      args:
        creates: "{{ (lfs_main_dir, 'lib', 'libncursesw.so.{}'.format(package.version)) | path_join }}"

    - name: correct symlink
      file:
        state: link
        src: ../../lib/libncursesw.so.{{ package.version }}
        path: "{{ lfs_main_dir }}/usr/lib/libncursesw.so"

- name: build bash
  tags: [bash]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: bash
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --build=$(support/config.guess) \
          --host={{ lfs_target_name }} \
          --without-bash-malloc
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/bash"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/usr/bin/bash"

    - name: copy bash to /bin
      copy:
        remote_src: true
        src: "{{ lfs_main_dir }}/usr/bin/bash"
        dest: "{{ lfs_main_dir }}/bin"

    - name: create symlink sh
      file:
        state: link
        src: "bash"
        path: "{{ lfs_main_dir }}/bin/sh"

- name: build coreutils
  tags: [coreutils]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: coreutils
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --host={{ lfs_target_name }} \
          --build=$(build-aux/config.guess) \
          --enable-install-program=hostname \
          --enable-no-install-program=kill,uptime
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      community.general.make:
        chdir: "{{ package_dir }}"
        jobs: "{{ ansible_processor_cores }}"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/usr/bin/ls"

    - name: copy binaries to /bin
      copy:
        remote_src: true
        src: "{{ (lfs_main_dir, 'usr/bin', item) | path_join }}"
        dest: "{{ lfs_main_dir }}/bin"
      loop:
        - cat
        - chgrp
        - chmod
        - chown
        - cp
        - date
        - dd
        - df
        - echo
        - "false"
        - "true"
        - ln
        - ls
        - mkdir
        - mknod
        - mv
        - pwd
        - rm
        - rmdir
        - stty
        - sync
        - uname
        - head
        - nice
        - sleep
        - touch

    - name: copy binaries to /usr/sbin
      copy:
        remote_src: true
        src: "{{ (lfs_main_dir, 'usr/bin', item) | path_join }}"
        dest: "{{ lfs_main_dir }}/usr/sbin"
      loop:
        - chroot

    - name: create required man dir
      file:
        state: directory
        path: "{{ lfs_main_dir }}/usr/share/man/man8"

    - name: move chroot man page
      vars:
        chroot_oldpath: "{{ lfs_main_dir }}/usr/share/man/man1/chroot.1"
        chroot_newpath: "{{ lfs_main_dir }}/usr/share/man/man8/chroot.8"
      block:
        - name: check if chroot is in man1
          stat:
            path: "{{ chroot_oldpath }}"
          register: old_chroot_stat

        - name: copy chroot man page
          copy:
            remote_src: true
            src: "{{ chroot_oldpath}}"
            dest: "{{ chroot_newpath}}"
          when: old_chroot_stat.stat.exists

        - name: remote chroot old man page
          file:
            state: absent
            path: "{{ chroot_oldpath }}"
          when: old_chroot_stat.stat.exists

        - name: correct chroot man page
          replace:
            path: "{{ chroot_newpath }}"
            regexp: '"1"'
            replace: '"8"'

- name: build diffutils
  tags: [diffutils]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: diffutils
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      command: ./configure --prefix=/usr --host={{ lfs_target_name }}
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/src/diff"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }}
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/usr/bin/diff"

- name: build file
  tags: [file]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: file
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: create build dir
      file:
        state: directory
        path: "{{ package_dir }}/build"

    - name: configre sub-makefile
      shell: |
        ../configure --disable-bzlib \
          --disable-libseccomp \
          --disable-xzlib \
          --disable-zlib
      args:
        chdir: "{{ package_dir }}/build"
        creates: "{{ package_dir }}/build/Makefile"

    - name: compile sub-makefile
      community.general.make:
        chdir: "{{ package_dir }}/build"
        jobs: "{{ ansible_processor_cores }}"

    - name: configure makefile
      shell: ./configure --prefix=/usr --host={{ lfs_target_name }} --build=$(./config.guess)
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j FILE_COMPILE={{ package_dir }}/build/src/file
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/src/file"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/usr/bin/file"

- name: build findutils
  tags: [findutils]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: findutils
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --host={{ lfs_target_name }} \
          --build=$(build-aux/config.guess)
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/find/find"

    - name: install
      shell: |
        make -j DESTDIR={{ lfs_main_dir }} install
        mv {{ lfs_main_dir }}/usr/bin/find {{ lfs_main_dir }}/bin
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/bin/find"

    - name: update config for updatedb
      replace:
        path: "{{ lfs_main_dir }}/usr/bin/updatedb"
        regexp: "find:=${BINDIR}"
        replace: "find:=/bin"

- name: build gawk
  tags: [gawk]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: gawk
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: update configure config
      replace:
        path: "{{ package_dir }}/Makefile.in"
        regexp: extras
        replace: ""

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --host={{ lfs_target_name }} \
          --build=$(./config.guess)
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/gawk"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/usr/bin/gawk"

- name: build grep
  tags: [grep]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: grep
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --host={{ lfs_target_name }} \
          --bindir=/bin
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/src/grep"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/bin/grep"

- name: build gzip
  tags: [gzip]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: gzip
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      command: ./configure --prefix=/usr --host={{ lfs_target_name }}
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/gzip"

    - name: install
      shell: |
        make -j DESTDIR={{ lfs_main_dir }} install
        mv {{ lfs_main_dir }}/usr/bin/gzip {{ lfs_main_dir }}/bin
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/bin/gzip"

- name: build make
  tags: [make]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: make
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --without-guile \
          --host={{ lfs_target_name }} \
          --build=$(build-aux/config.guess)
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/make"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/usr/bin/make"

- name: build patch
  tags: [patch]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: patch
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --host={{ lfs_target_name}} \
          --build=$(build-aux/config.guess)
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      community.general.make:
        chdir: "{{ package_dir }}"
        jobs: "{{ ansible_processor_cores }}"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/usr/bin/patch"

- name: build sed
  tags: [sed]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: sed
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --host={{ lfs_target_name }} \
          --bindir=/bin
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/sed"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/bin/sed"

- name: build tar
  tags: [tar]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: tar
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --host={{ lfs_target_name }} \
          --build=$(build-aux/config.guess) \
          --bindir=/bin
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      community.general.make:
        chdir: "{{ package_dir }}"
        jobs: "{{ ansible_processor_cores }}"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/bin/tar"

- name: build xz
  tags: [xz]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: xz
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: configure makefile
      shell: |
        ./configure --prefix=/usr \
          --host={{ lfs_target_name }} \
          --build=$(build-aux/config.guess) \
          --disable-static \
          --docdir=/usr/share/doc/xz-{{ package.version }}
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ package_dir }}/src/xz/xz"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}"
        creates: "{{ lfs_main_dir }}/usr/bin/xz"

    - name: copy file to another location
      copy:
        remote_src: yes
        src: "{{ (lfs_main_dir, 'usr/bin', item) | path_join }}"
        dest: "{{ lfs_main_dir }}/bin"
      loop:
        - lzma
        - unlzma
        - lzcat
        - xz
        - unxz
        - xzcat

    - name: move lib file
      shell: |
        mv {{ lfs_main_dir }}/usr/lib/liblzma.so.* {{ lfs_main_dir }}/lib
      args:
        creates: "{{ lfs_main_dir }}/lib/liblzma.so.{{ package.version }}"

    - name: update link to lib
      file:
        state: link
        src: "../../lib/liblzma.so.{{ package.version }}"
        path: "{{ lfs_main_dir }}/usr/lib/liblzma.so"

- name: build binutils v2
  tags: [binutils, binutils-v2]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: binutils
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archive
      unarchive:
        src: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
        dest: "{{ lfs_sources_dir }}"
        creates: "{{ package_dir }}"

    - name: create build dir
      file:
        state: directory
        path: "{{ package_dir }}/build-v2"

    - name: configure makefile
      shell: |
        ../configure \
          --prefix=/usr \
          --build=$(../config.guess) \
          --host={{ lfs_target_name }} \
          --disable-nls \
          --enable-shared \
          --disable-werror \
          --enable-64-bit-bfd
      args:
        chdir: "{{ package_dir }}/build-v2"
        creates: "{{ package_dir }}/build-v2/Makefile"

    - name: compile
      command: make -j
      args:
        chdir: "{{ package_dir }}/build-v2"
        creates: "{{ package_dir }}/build-v2/libctf/.libs/libctf.so.0.0.0"

    - name: install
      command: make -j DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}/build-v2"
        creates: "{{ lfs_main_dir }}/usr/x86_64-lfs-linux-gnu/lib"

- name: build gcc v2
  tags: [gcc, gcc-v2]
  hosts: host
  become: yes
  become_user: "{{ lfs_user.name }}"
  vars:
    package_name: gcc
    package: "{{ lfs_packages[package_name] }}"
    package_dir: "{{ lfs_sources_dir }}/{{ '{}-{}'.format(package_name, package.version) }}"

  tasks:
    - name: extract archives
      unarchive:
        remote_src: yes
        src: "{{ item.source }}"
        dest: "{{ item.dest if item.dest is defined else lfs_sources_dir }}"
        creates: "{{ item.creates }}"
      loop:
        - source: "{{ lfs_sources_dir }}/{{ package.url | basename }}"
          creates: "{{ package_dir }}"

        - source: "{{ lfs_sources_dir }}/mpfr-4.1.0.tar.xz"
          dest: "{{ package_dir }}"
          creates: "{{ package_dir }}/mpfr-4.1.0"

        - source: "{{ lfs_sources_dir }}/gmp-6.2.1.tar.xz"
          dest: "{{ package_dir }}"
          creates: "{{ package_dir }}/gmp-6.2.1"

        - source: "{{ lfs_sources_dir }}/mpc-1.2.1.tar.gz"
          dest: "{{ package_dir }}"
          creates: "{{ package_dir }}/mpc-1.2.1"

    - name: create links
      file:
        state: link
        src: "{{ item.src }}"
        path: "{{ item.path }}"
      loop:
        - path: "{{ package_dir }}/mpfr"
          src: "{{ package_dir }}/mpfr-4.1.0"

        - path: "{{ package_dir }}/gmp"
          src: "{{ package_dir }}/gmp-6.2.1"

        - path: "{{ package_dir }}/mpc"
          src: "{{ package_dir }}/mpc-1.2.1"

    - name: change lib path for 64 bits
      replace:
        path: "{{ package_dir }}/gcc/config/i386/t-linux64"
        regexp: '(m64=.*/lib)64?'
        replace: '\1'
        backup: true
      when: ansible_architecture == 'x86_64'

    - name: create required directory
      file:
        state: directory
        path: "{{ item }}"
        recurse: true
      loop:
        - "{{ package_dir }}/build-v2"
        - "{{ (package_dir, 'build-v2', lfs_target_name, 'libgcc') | path_join }}"

    - name: create link to posix thread
      file:
        state: link
        src: ../../../libgcc/gthr-posix.h
        path: "{{ (package_dir, 'build-v2', lfs_target_name, 'libgcc/gthr-default.h') | path_join }}"

    - name: configure makefile
      shell: |
        ../configure \
          --build=$(../config.guess) \
          --host={{ lfs_target_name }} \
          --prefix=/usr \
          CC_FOR_TARGET={{ lfs_target_name }}-gcc \
          --with-build-sysroot={{ lfs_main_dir }} \
          --enable-initfini-array \
          --disable-nls \
          --disable-multilib \
          --disable-decimal-float \
          --disable-libatomic \
          --disable-libgomp \
          --disable-libquadmath \
          --disable-libssp \
          --disable-libvtv \
          --disable-libstdcxx \
          --enable-languages=c,c++
      args:
        creates: "{{ package_dir }}/build-v2/Makefile"
        chdir: "{{ package_dir }}/build-v2"

    - name: compile
      command: make
      args:
        chdir: "{{ package_dir }}/build-v2"
        creates: "{{ package_dir }}/build-v2/gcc/nm"

    - name: install
      command: make DESTDIR={{ lfs_main_dir }} install
      args:
        chdir: "{{ package_dir }}/build-v2"
        creates: "{{ lfs_main_dir }}/usr/bin/x86_64-lfs-linux-gnu-gcc"

    - name: create cc compiler link
      file:
        state: link
        src: gcc
        path: "{{ lfs_main_dir }}/usr/bin/cc"
