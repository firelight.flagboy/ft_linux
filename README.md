# ft_linux

"how to train your kernel" \
Make your own linux distro

## Instruction

1. For this subject, you must use a virtual machine, live `VirtualBox` or `VMWare`.
2. Though it is not **REQUIRED**, you **SHOULD** read [this](http://pubs.opengroup.org/onlinepubs/9699919799/) and [that](http://refspecs.linuxfoundation.org/lsb.shtml) right now. \
  Keep those standards in mind. \
  You won’t be graded on your compliance with them, \
  but still, it would be good practice.
3. You must use a kernel version `4.x`. Stable or not, as long as it’s a `4.x` version.
4. The kernel sources must be in `/usr/src/kernel-<version>`
5. You must use at least **3** differents partitions. (`root`, `/boot` and a `swap` partition). \
  You can of course make more partitions if you want to.
6. Your distro must implement a *kernel_module* loader, like `udev`.
7. The kernel version must contain your student login in it. Something like `Linux kernel 4.1.2-<student_login>`
8. The distribution `hostname` must be your student login
9. You’re free to choose between a **32** or **64-bit** system.
10. You must use a sofware for central management and configuration, like `SysV` or `SystemD`.
11. Your distro must boot with a *bootloader*, like `LILO` or `GRUB`.
12. The kernel binary located in `/boot` must be named like this:
    `vmlinuz-<linux_version>-<student_login>` \
    Adapt your bootloader configuration to that.

### Instruction Todo

| task                                                  | comments                                                  | state | How to check                          |
| ----------------------------------------------------- | --------------------------------------------------------- | ----- | ------------------------------------- |
| Virtualization (1)                                    | `virtualbox` + `vagrant`                                  | ✔     |                                       |
| Kernel version (3)                                    | `5.x`                                                     | ✔     | `uname -r`                            |
| Kernel src `/usr/src/kernel-<version>` (4)            |                                                           | ✔     | `ls /usr/src/kernel-*`                |
| You use 3 differents partitions (5)                   | `/boot`, `root`, `swap`                                   | ✔     | `lsblk`                               |
| Kernel module loader (6)                              | `udev`                                                    | ✔     | `installed by systemctl`              |
| Kernel version contain your login (7)                 |                                                           | ✔     | `cat /proc/version`                   |
| The distribution `hostname` is your login (8)         | see [lfs](/group_vars/lfs/generic.yml#L1) [login](/login) | ✔     | `hostname`                            |
| System arch (9)                                       | `64bits`                                                  | ✔     | `uname -m`                            |
| sofware for central management and configuration (10) | `systemd`                                                 | ✔     | `systemctl list-units --type=service` |
| Your distro must boot with a *bootloader* (11)        | `grub`                                                    | ✔     | `whereis grub`                        |
| The kernel bin name respect to template (12)          |                                                           | ✔     | `ls /boot/vmlinuz-*-*`                |

## Source

- [subject](https://cdn.intra.42.fr/pdf/pdf/13374/en.subject.pdf)
- [Linux From Scratch - 10.1](http://www.linuxfromscratch.org/lfs/downloads/10.1-systemd/LFS-BOOK-10.1-systemd-NOCHUNKS.html)
- [Beyond Linux From Scratch - 10.1](https://www.linuxfromscratch.org/blfs/view/stable-systemd/index.html)
  - libtasn1
  - p11-kit
  - make-ca
  - openssh
  - curl
  - wget
